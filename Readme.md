# React/Redux/ES6 Boilerplate

A React/Preact/Inferno/Redux/Universal Webpack/SSR/Code Splitting/Docker/Buzzword/boilerplate/tutorial peppered with explanatory comments.

## Features:

- Universal javascript for all 3 Frameworks using -compat libraries (and Webpack magic)
- Server side rendering
- Easy switching of frameworks in dev and prod builds with `FRAMEWORK` env variable.
- Code splitting with react-router
- Webpack for the server side
- Webpack 2 tree shaking, code splitting, dead code elimination
- Simple npm run commands
- Webpack dev watching and rebuilding
- Closure compiler with Webpack
- Webpack bundle size analysis plugin
- Docker for both dev and prod builds/running
- Eslint w/Airbnb (very rough working config)
- SCSS build to one file (rough working config, no PostCSS etc.)

Actual react/redux app code is bad/a quick hack prototype. (anything within components/container/redux to be thrown away/not to judge me on!).

## Basics:

Run the usual:

`yarn install` or `npm install`

`npm run dev` - runs a webpack watch on both client and server code and bundles it to build/client and build/server respectively. Also starts and restarts server on changes, listening on port 3000.

`npm run prod` does the same as dev, but with production plugins enabled, `NODE_ENV=production` and watching disabled.

To start the server: `npm start` for production mode.

Server will then be listening on port 3000.

Can also use Docker to run in both prod and dev - Jump to Docker section for equivalent docker-compose development and production running commands.

## Frameworks:
React, Preact and Inferno compared (with and without Google Closure Compiler).

Can easily switch between these 3 frameworks by specifying a `FRAMEWORK` environment variable to the `npm run dev` or `npm run prod` commands, eg: `FRAMEWORK=preact npm run prod`. Defaults to React if not specified.

Bundle Sizes for `vendor.js` for each framework (and with using Google Closure compiler):

Vendor contains: `'react'`, `'react-dom'`, `'react-redux'`, `'react-router'`, `'react-router-redux'`, `'redux-saga'` or equivalent compat libraries so we can see the "real world saving" in our bundle (as shaving 90% off a small percentage of our bundle is a less useful statistic).

|                          | React (kb) | Clojure Compiler Saving % | Preact (kb) | % Saving from React | Clojure Compiler Saving | Inferno (kb) | % Saving from React | Clojure Compiler Saving % |
|--------------------------|------------|---------------------------|-------------|---------------------|-------------------------|--------------|---------------------|---------------------------|
| Vendor                   | 292        |                           | 140         | 52.05%              |                         | 155          | 46.92%              |                           |
| Vendor (Clojure)         | 238        | 18.49%                    | 120         | 49.58%              | 14.29%                  | 130          | 45.38%              | 16.13%                    |
| Vendor Gzipped           | 73.9       |                           | 36.8        | 50.20%              |                         | 41.4         | 43.98%              |                           |
| Vendor (Clojure Gzipped) | 67.5       | 8.66%                     | 34.9        | 48.30%              | 5.16%                   | 39           | 42.22%              | 5.80%                     |

Detailed breakdown of each outputted webpack file can be found in `stats.html` located in the build file after building.

## Docker:

Docker-compose is essentially the start script for the application, and there are 2 compose files (dev and prod) so that dev can mount the local filesystem for hot reloading (copying the files usually happens at buildtime in the Dockerfile) and with different environment variables.

`docker-compose build`
Builds all the applications in the docker-compose.yml file (if servicename not provided) and tags with latest using foldername_servicename as the resulting image name (unless image is specified in the docker-compose).

`docker-compose up`
Runs the built image with `npm start` (also the default in the .Dockerfile) in prod mode inside a docker container.

`docker-compose -f docker-compose-dev.yml up` - dev mode within a docker container.

'-f' means path to config file, which brings up the application using settings defined in the docker-compose-dev.yml file.

If you're not using docker-compose and instead want to use "vanilla docker":

`docker build -t {tagname} .` - to build the image
`docker run -d -p 3000:3000 {tagname}` - to run in production inside container (maps port 3000 in the container to port 3000 on your machine). -d means it's detached and you won't see container output in your shell.

Dev mode cannot be done as easily, as there is a mounting of the local filesystem to the docker container specified in the `docker-compose-dev.yml` file.

### *Note: Yarn*

Using yarn for quick installs etc of node_modules when developing locally (not using docker), but it becomes a bit irrelevant in CI pipeline if using docker, given the "cached layers" nature of docker. I tried yarn inside docker for even more speed, but had issues. Would be nice in future due to the yarn.lock file ensuring the exact dependency versions across docker/local development.

# Webpack

*My favourite part... more difficult than this little section would lead you to believe...*

1 Webpack.config.js file for simplicity. *"Universal"* processing both client and server code.

`"modules": false` in the .babelrc es2015 preset enables webpack tree shaking and dead code elimination.

Toggles are `NODE_ENV` and `FRAMEWORK` and affect the clientPlugins and serverPlugins variables, performance hints, devtools (sourcemaps) and in future hot module replacement.

Bundles server into 1 file, with "externals" making it so that the node\_modules are not bundled into the server code. Exceptions are made for react/preact/inferno libraries (they are bundled into the server code as a result) so that the webpack aliases don't break and work on the server side.

*Need to benchmark the performance vs transpiled src and server folder.*

### Code Splitting

This is defined in the routes folder using react-router and the getComponent method and resolved by webpack at build time.

It is done by using require.ensure([], require => require('containers/Pages/Home'), 'home') with the final paramater becoming the webpack chunk name. (If you give 2 routes the same chunk name they will be merged). The main client code then looks up the output chunk name with its path relative to outPutPath set in webpack and loads it on route change.

As a further optimisation, the same can be done with the reducers: https://github.com/insin/react-examples/tree/master/code-splitting-redux-reducers (currently not implemented)

## TODO/Improvements:

- Test performance of bundled server code vs transpiled files.
- Tidy up unused packages from package.json
- Webpack dev build to memory rather than fs (faster)
- Webpack hot reloading of server instead of complete update and restart
- Webpack dev server for assets
- Better SCSS webpack processing
- Docker testing process etc.
- Testing? :(

#### Misc:

Some Useful links:
- https://github.com/halt-hammerzeit/webpack-react-redux-isomorphic-render-example
- https://moduscreate.com/webpack-2-tree-shaking-configuration/
- https://github.com/ReactTraining/react-router/blob/master/docs/guides/ServerRendering.md
- https://github.com/webpack/webpack/issues/1599 - dirname & paths fix


May be possible to alias preact etc whilst using the node_modules folder by using webpack.NormalModuleReplacementPlugin instead of removing it from externals, and bundling it in to the server code.
