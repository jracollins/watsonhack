import { TEST_ACTION, TEST_ACTION_2 } from './constants';

const initialState = {
  version: 1
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case TEST_ACTION:
      return {
        ...state,
        version: state.version + 1
      };
    case TEST_ACTION_2:
      return {
        ...state,
        test: 'bye'
      };
    default:
      return state;
  }
}
