import map from 'lodash/map';

import { TEST_ACTION, TEST_ACTION_2 } from './constants';

const summaries = require('./expandedSummaries.json');

function mapSummariesToProducts(expandedSummaryResponse) {
  const formatted = {};

  map(expandedSummaryResponse, (product) => {
    formatted[product.id] = {
      name: product.name,
      price: product.price,
      onSale: product.onSale,
      brand: product.brand,
      images: product.images,
      badges: product.badges
    };
  });

  return formatted;
}


const initialState = mapSummariesToProducts(summaries.summaries);

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case TEST_ACTION:
      return {
        ...state,
        version: state.version + 1
      };
    case TEST_ACTION_2:
      return {
        ...state,
        test: 'bye'
      };
    default:
      return state;
  }
}
