import { TEST_ACTION, TEST_ACTION_2 } from './constants';

export function testAction1() {
  return {
    type: TEST_ACTION
  };
}

export function testAction2() {
  return {
    type: TEST_ACTION_2
  };
}
