import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import configReducer from 'app/redux/ducks/config/reducer';
import productlists from 'app/redux/ducks/productlists/reducer';
import products from 'app/redux/ducks/products/reducer';

export default combineReducers({
  config: configReducer,
  routing: routerReducer,
  products,
  productlists
});
