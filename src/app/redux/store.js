import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';

const isBrowser = typeof window !== 'undefined';

export default function configureStore(state) {
  const sagaMiddleware = createSagaMiddleware();
  const intitRouterMiddleware = routerMiddleware(browserHistory);
  // https://github.com/zalmoxisus/redux-devtools-extension#usage
  let composeEnhancers = compose;

  const store = createStore(reducers, state, composeEnhancers(applyMiddleware(sagaMiddleware, intitRouterMiddleware)));

  // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers').default;
      store.replaceReducer(connectRouter(history)(nextRootReducer)); // Need connectRouter to mount router state
    });
  }

  return store;
}
