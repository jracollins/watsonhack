import React, { PropTypes } from 'react';
import Match from 'react-router/Match';
import Redirect from 'react-router/Redirect';
import Miss from 'react-router/Miss';
import AppContainer from 'app/containers/AppContainer';
import { Home, ProductList, ProductPage } from 'app/containers';

const App = props => (
  <AppContainer>
    <Match exactly pattern="/" component={Home} />
    <Miss component={NoMatch} />
  </AppContainer>
  );

const NoMatch = () => (<Redirect to="/" />);

export default App;
