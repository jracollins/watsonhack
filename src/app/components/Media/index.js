import React, { Component, PropTypes } from 'react';
import './media.scss';

export default class Media extends Component {
  render() {
    return (
      <div className="media" style={{ paddingTop: `${this.props.padding}%` }}>
        <img alt={this.props.alt} src={this.props.src} />
      </div>
    );
  }
}
