import React, { PropTypes } from 'react';
import { Row } from 'app/components/Layout';
import './index.scss';

const TopNav = props => (
  <div className="top-nav flex-container">
    <Row sm={props.sm} md={props.md} lg={props.lg} xlg={props.xlg} className="productlist-products">
      {props.children}
    </Row>
  </div>
);


TopNav.defaultProps = {
};


TopNav.propTypes = {

};


export default TopNav;
