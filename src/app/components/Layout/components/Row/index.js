import React, { PropTypes } from 'react';
import classname from 'classname';
import './index.scss';

const Row = props => (
  <div className="flex-container">
    <div className={classname('flex-row', cssClassList(props))}>
      {props.children}
    </div>
  </div>
);


const classStringGenerator = (size, columnCount) => `${size}-flex-${columnCount}-columns`;

const cssClassList = (props) => {
  const cssClasses = [];

  if (props.xs) {
    cssClasses.push(classStringGenerator('xs', props.xs));
  }

  if (props.sm) {
    cssClasses.push(classStringGenerator('sm', props.sm));
  }

  if (props.md) {
    cssClasses.push(classStringGenerator('md', props.md));
  }

  if (props.lg) {
    cssClasses.push(classStringGenerator('lg', props.lg));
  }

  if (props.xlg) {
    cssClasses.push(classStringGenerator('xlg', props.xlg));
  }

  if (props.className) {
    cssClasses.push(props.className);
  }

  return cssClasses;
};


export default Row;
