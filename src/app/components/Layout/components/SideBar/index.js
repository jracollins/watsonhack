import React, { PropTypes } from 'react';
import './index.scss';

const SideBar = props => (
  <div id="side-bar" className="flex-container">
    <div className={''}>
      {props.children}
    </div>
  </div>
);


SideBar.defaultProps = {
};


SideBar.propTypes = {

};


export default SideBar;
