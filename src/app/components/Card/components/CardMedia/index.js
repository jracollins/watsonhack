import React, { PropTypes } from 'react';
import Media from 'app/components/Media';
import './index.scss';

CardMedia.propTypes = {
  url: PropTypes.string,
  alt: PropTypes.string,
  padding: PropTypes.number,
  children: PropTypes.array
};

const CardMedia = props => (
  <div className="card-media">
    <Media alt={props.alt} src={props.url} padding={props.padding} />
    <div className="card-media-children">
      {props.children}
    </div>
  </div>
);

export default CardMedia;
