import React from 'react';
import './index.scss';

const CardDivider = () => (
  <div className="card-divider" />
);

export default CardDivider;
