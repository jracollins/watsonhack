import React, { PropTypes } from 'react';
import './index.scss';

CardText.propTypes = {
  text: PropTypes.string,
};

const CardText = props => (
  <div className="card-text">{props.text}</div>
);

export default CardText;
