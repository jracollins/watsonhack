import React, { PropTypes } from 'react';
import './index.scss';

CardTitle.propTypes = {
  title: PropTypes.string,
};

const CardTitle = props => (
  <div className="card-title">{props.title}</div>
);

export default CardTitle;
