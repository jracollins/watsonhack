import React, { PropTypes } from 'react';
import './card.scss';

const Card = props => (
  <div className="card">
    <div className="card-content">
      {props.children}
    </div>
  </div>
);

Card.propTypes = {
  children: PropTypes.object,
};

export default Card;
export { CardMedia, CardText, CardTitle, CardDivider } from './components';
