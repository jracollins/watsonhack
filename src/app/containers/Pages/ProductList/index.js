import React, { Component, PropTypes } from 'react';

class ProductListContainer extends Component {

  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div className="productlist">
        <h1>PRODUCTLIST</h1>
        <h2>{this.props.location.pathname}</h2>
      </div>
    );
  }
}

export default ProductListContainer;
