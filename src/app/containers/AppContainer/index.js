import React, { Component, PropTypes } from 'react';
import { Link, Redirect, propTypes } from 'react-router';
import Layout, { TopNav, Row } from 'app/components/Layout';
import Card, { CardMedia, CardText, CardTitle } from 'app/components/Card';

import { SpeechToText } from 'watson-speech';
import map from 'lodash/map';
import filter from 'lodash/filter';
// import indexOf from 'lodash/indexOf';
import superagent from 'superagent';
import keymirror from 'keyMirror';

const { recognizeMicrophone } = SpeechToText;

const keywords = ['yes', 'no', 'next', 'expensive'];

const brands = require('./brands.js');

const brandIdMap = {};
map(brands, (brand) => {
  Object.assign(brandIdMap, { [brand.id]: brand.name });
});

const idBrandMap = keymirror(brandIdMap);

const colours = require('./colours.js');

const colourIdMap = {};

map(colours, (colour) => {
  Object.assign(colourIdMap, { [colour.id]: colour.name });
});

const idColourMap = keymirror(colourIdMap);

// const categories = require('./categories').categories;
//
// const categoryIdMap = {};
//
// map(categories, (category) => {
//   Object.assign(categoryIdMap, { [category.id]: category.name });
// });
//
// map(categories, (category) => {
//   childrenToCategoryIdMap(category);
// });
//
// const idCategoryMap = keymirror(categoryIdMap);
//
// function childrenToCategoryIdMap(category) {
//   if (category && category.id) {
//     if (category.name) {
//       Object.assign(categoryIdMap, { [category.id]: category.name });
//     }
//     if (category.children) {
//       map(category.children, (categoryChild) => {
//         childrenToCategoryIdMap(categoryChild);
//       });
//     }
//
//   } else if (category && category.children) {
//
//     childrenToCategoryIdMap(category);
//   }
//
// }
//
// console.log(categoryIdMap);


class App extends Component {
  static propTypes = {};

  static contextTypes = {
    router: propTypes.routerContext
  }

  constructor(props) {
    super(props);
    this.state = {
      token: 'No token yet...',
      rawMessages: [],
      formattedMessages: [],
      allFormattedMessages: [],
      listening: false,
      settingsAtStreamStart: {
        model: 'en-US_BroadbandModel',
      },
      product: null,
      likedBrands: [],
      dislikedBrands: [],
      likedColours: [],
      dislikedColours: [],
      topPriceRange: 500000,
      bottomPriceRange: 20000,
      likedCategories: [],
      dislikedCategories: [],
      ladPageOffset: 0,
    };
  }

  componentDidMount() {
    // Getting token and saving to state
    superagent
      .get('/token')
      .then((response) => {
        console.log('*********');
        console.log(response);
        this.setState({ token: response.text }, this.toggleMic);
      });

    this.getProducts();

    // Prevent any route transitions from xf
    const aTags = document.getElementsByTagName('a');
    for (let i = 0; i < aTags.length; i++) {
      aTags[i].addEventListener('click', (event) => {
        event.preventDefault();
      });
    }
  }

  toggleMic() {
    if (this.state.listening) {
      this.stopTranscription();
    } else {
      this.handleStream(recognizeMicrophone({
        token: this.state.token,
        interim_results: true,
        continuous: true,
        keywords,
        keywords_threshold: 0.01,
        word_alternatives_threshold: 0.01,
        format: true }));
    }
  }

  handleStream(stream) {
    if (this.stream) {
      this.stream.stop();
      this.stream.removeAllListeners();
      this.stream.recognizeStream.removeAllListeners();
    }
    this.stream = stream;
    console.log('Starting listening');
    this.setState({ listening: true });

    const messageFormatter = this.handleFormattedMessage.bind(this);
    const errorHandler = this.handleError.bind(this);
    const stopTranscriptionHandler = this.stopTranscription.bind(this);
    // grab the formatted messages and also handle errors and such
    stream.on('data', messageFormatter).on('end', stopTranscriptionHandler).on('error', errorHandler);

    // when errors occur, the end event may not propagate through the helper streams.
    // However, the recognizeStream should always fire a end and close events
    stream.recognizeStream.on('end', () => {
      if (this.state.error) {
        stopTranscriptionHandler();
      }
    });

    // grab raw messages from the debugging events for display on the JSON tab
    stream.recognizeStream
      .on('message', (frame, json) => this.handleRawdMessage({ sent: false, frame, json }))
      .on('send-json', json => this.handleRawdMessage({ sent: true, json }))
      .once('send-data', () => this.handleRawdMessage({
        sent: true, binary: true, data: true // discard the binary data to avoid waisting memory
      }))
      .on('close', (code, message) => this.handleRawdMessage({ close: true, code, message }));

    // ['open','close','finish','end','error', 'pipe'].forEach(e => {
    //     stream.recognizeStream.on(e, console.log.bind(console, 'rs event: ', e));
    //     stream.on(e, console.log.bind(console, 'stream event: ', e));
    // });

  }

  handleRawdMessage(msg) {
    this.setState({ rawMessages: this.state.rawMessages.concat(msg) });
  }

  handleFormattedMessage(msg) {
    this.setState({ allFormattedMessages: this.state.allFormattedMessages.concat(msg.toString()) }, this.handleKeyword);

    if (this.state.isActive) {
      this.setState({ formattedMessages: this.state.formattedMessages.concat(msg.toString()) }, this.handleCommand);
    }
  }

  handleKeyword() {
    if (this.state.allFormattedMessages[0].match(/(mr p)/gi)) {
      this.setState({ isActive: true });
      console.log('mr p is listening');
    }

    if (this.state.allFormattedMessages[0].match(/stop/gi)) {
      this.setState({ isActive: false });
      console.log('mr p is not listening');
    }

    this.setState({ allFormattedMessages: [] });
  }

  handleCommand() {
    this.nextPage();
    this.previousPage();
    this.brand();
    // this.likeCategory();
    this.tooExpensive();
    this.tooCheap();
    this.matchColour();
    this.setState({ formattedMessages: [] });
  }

  nextPage() {
    if (this.state.formattedMessages[0].match(/(next|right)/gi)) {
      this.setState({ ladPageOffset: this.state.ladPageOffset + 1 }, this.getProducts);
    }
  }

  previousPage() {
    if (this.state.formattedMessages[0].match(/(back|previous|left)/gi)) {
      this.setState({ ladPageOffset: this.state.ladPageOffset - 1 }, this.getProducts);
    }
  }

  brand() {
    console.log(this.state.formattedMessages[0]);

    if (this.state.formattedMessages[0].match(/(brand|designer|design)/gi)) {
      this.alchemy()
        .then((sentiment) => {
          if (sentiment > 0.05) {

            if (this.state.ladPageOffset === 0) {
              this.state.ladPageOffset += 1;
            }


            this.setState({
              likedBrands: (this.state.likedBrands.indexOf(this.state.product.brandId) === -1) ? this.state.likedBrands.concat(this.state.product.brandId) : this.state.likedbrands,
              dislikedBrands: [] }, this.getProducts);
          } else {
            this.setState({ likedBrands: [], dislikedBrands: this.state.dislikedBrands.concat(this.state.product.brandId) }, this.getProducts);
          }
        });
    }
  }

  likeCategory() {
    if (this.state.formattedMessages[0].match(/(category|stuff)/gi)) {
      this.alchemy()
        .then((sentiment) => {
          if (sentiment > 0.05) {
            if (this.state.ladPageOffset === 0) {
              this.state.ladPageOffset += 1;
            }
            this.setState({
              likedCategories: this.state.likedCategories.concat(this.state.product.leafCategoryIds[0]),
              dislikedCategories: [] }, this.getProducts);
          } else {
            this.setState({ likedCategories: [], dislikedCategories: this.state.dislikedCategories.concat(this.state.product.brandId) }, this.getProducts);
          }
        });
    }
  }

  tooExpensive() {
    if (this.state.formattedMessages[0].match(/(dear|expensive)/gi)) {

      const newAmount = this.state.product.price.amount - 20000;

      if (newAmount < this.state.bottomPriceRange) {
        this.setState({ bottomPriceRange: (newAmount - 40000) > 1 ? newAmount - 40000 : newAmount - 40000 });
      }


      this.setState({ topPriceRange: newAmount }, this.getProducts);
    }
  }

  tooCheap() {
    if (this.state.formattedMessages[0].match(/(cheap|tacky)/gi)) {
      const newAmount = this.state.product.price.amount + 20000;

      if (newAmount > this.state.topPriceRange) {
        this.setState({ topPriceRange: newAmount + 40000 });
      }

      this.setState({ bottomPriceRange: newAmount }, this.getProducts);
    }
  }

  matchColour() {
    if (this.state.formattedMessages[0].match(/(color|colour)/gi)) {
      this.alchemy()
        .then((sentiment) => {
          if (sentiment > 0.05) {

            if (this.state.ladPageOffset === 0) {
              this.state.ladPageOffset += 1;
            }

            this.setState({ dislikedColours: [], likedColours: (this.state.likedColours.indexOf(this.state.product.colourIds[0]) === -1) ? this.state.likedColours.concat(this.state.product.colourIds[0]) : this.state.likedColours }, this.getProducts);
          } else {
            this.setState({ likedColours: [], dislikedColours: (this.state.dislikedColours.indexOf(this.state.product.colourIds[0]) === -1) ? this.state.dislikedColours.concat(this.state.product.colourIds[0]) : this.state.dislikedColours }, this.getProducts);
          }
        });
    }
  }


  stopTranscription() {
    console.log('Stopping listening');
    this.stream.stop();
    this.setState({ listening: false });
  }


  handleError(err, extra) {
    console.error(err, extra);
    if (err.name == 'UNRECOGNIZED_FORMAT') {
      err = 'Unable to determine content type from file header; only wav, flac, and ogg/opus are supported. Please choose a different file.';
    } else if (err.name === 'NotSupportedError' && this.state.audioSource === 'mic') {
      err = 'This browser does not support microphone input.';
    } else if (err.message === '(\'UpsamplingNotAllowed\', 8000, 16000)') {
      err = 'Please select a narrowband voice model to transcribe 8KHz audio files.';
    }
    this.setState({ error: err.message || err });
    this.stopTranscription();
  }

  getProducts() {
    let brandIds = '';
    if (this.state.likedBrands.length > 0) {
      brandIds = `&brandIds=${this.state.likedBrands.join(',')}`;
    } else if (this.state.dislikedBrands.length > 0) {
      // const allBrandsExceptDiliked =
      const filteredBrands = filter(idBrandMap, brand => this.state.dislikedBrands.indexOf(Number(brand)) === -1);
      brandIds = `&brandIds=${filteredBrands.join(',')}`;
    }

    let colourIds = '';
    if (this.state.likedColours.length > 0) {
      colourIds = `&colourIds=${this.state.likedColours.join(',')}`;
    } else if (this.state.dislikedColours.length > 0) {
      // const allColoursExceptDiliked =
      const filteredColours = filter(idColourMap, colour => this.state.dislikedColours.indexOf(Number(colour)) === -1);
      colourIds = `&colourIds=${filteredColours.join(',')}`;
    }


    let categoryIds = '';
    if (this.state.likedCategories.length > 0) {
      categoryIds = `&categoryIds=${this.state.likedCategories.join(',')}`;
    } else if (this.state.dislikedCategories.length > 0) {
      // const allCategoriesExceptDiliked =
      const filteredCategories = filter(idCategoryMap, category => this.state.dislikedCategories.indexOf(Number(category)) === -1);
      categoryIds = `&categoryIds=${filteredCategories.join(',')}`;
    }


    const ladURL = `//lad-api.net-a-porter.com:80/MRP/GB/en/1/${this.state.ladPageOffset}/summaries?priceMin=${this.state.bottomPriceRange}&priceMax=${this.state.topPriceRange}&visibility=visible${brandIds}${colourIds}`;
    console.log(ladURL);
    superagent
      .get(ladURL)
      .end((err, response) => {
        console.log('*********');
        console.log(response.body.summaries[0]);
        this.setState({ product: response.body.summaries[0] });
        this.setState({ formattedMessages: [] });
      });
  }

  renderProduct() {
    const product = this.state.product;

    if (!product) {
      return;
    }

    const productImageUrl = formatImageTag(product.images.urlTemplate, product.images.shots[0], 'l');

    return (
      <Card key={product.id}>
        <CardTitle title={product.name} />
        <CardText text={`${brandIdMap[product.brandId]} | ${colourIdMap[product.colourIds[0]]} |  ${product.price.currency.replace('GBP', '£')}${product.price.amount / product.price.divisor}`} />
        <CardMedia url={productImageUrl} />
      </Card>
    );

  }

  alchemy(text) {
    return new Promise((resolve, reject) => {
      superagent
        .post('/alchemy')
        .send({ text: this.state.formattedMessages[0] })
        .then((response) => {

          const docEmotions = response.body.docEmotions;

          const { anger, disgust, fear, joy, sadness } = docEmotions;

          const totalNegative = ((Number(anger) + Number(disgust) + Number(fear) + Number(sadness)) / 4);
          console.log(totalNegative);
          console.log(joy);

          console.log(joy - totalNegative);

          resolve(joy - totalNegative);
        });
    });

  }

  renderBrands(brands) {
    const brandsToDiv = map(brands, (brand, key) => (<div key={key}>{brandIdMap[brand]}</div>));

    return (
      <div>
        {brandsToDiv}
      </div>
    );

  }


  renderColours(colours) {
    const coloursToDiv = map(colours, (colour, key) => (<div key={key}>{colourIdMap[colour]}</div>));

    return (
      <div>
        {coloursToDiv}
      </div>
    );

  }

  renderCategories(categoriesInput) {
    const categoriesToDiv = map(categoriesInput, (category, key) => (<div key={key}>{categoryIdMap[category]}</div>));

    return (
      <div>
        {categoriesToDiv}
      </div>
    );

  }

  render() {
    return (
      <div id="wrapper">

        <Row>
          <div id="header-title" className="center">
            {this.state.listening ? <div><span className="blink">🔴</span><span className="recording"> MR.P IS LISTENING</span></div> : <div><span className="not-recording-icon">🌑</span><span> NOT LISTENING</span></div>}
          </div>
          <div className="center">
            {this.state.isActive ? <div><span className="recording">MR P IS AWAITING YOUR COMMAND</span></div> : <div><span>SAY 'MR P' TO INTERACT</span></div>}
          </div>
          <div className="record-button" onClick={() => this.toggleMic()}>
            -
          </div>
        </Row>

        <Row md={2}>

          <Row md={1} className="product-detail">
            <div>
              <h2>LIKED DESIGNERS</h2>
              <span>{this.renderBrands(this.state.likedBrands)}</span>
            </div>
            <div>
              <h2>DISLIKED DESIGNERS</h2>
              <span>{this.renderBrands(this.state.dislikedBrands)}</span>
            </div>
            <div>
              <h2>LIKED COLOURS</h2>
              <span>{this.renderColours(this.state.likedColours)}</span>
            </div>
            <div>
              <h2>DISLIKED COLOURS</h2>
              <span>{this.renderColours(this.state.dislikedColours)}</span>
            </div>
            <div>
              <h2>PRICE RANGE</h2>
              <span>{`£${this.state.bottomPriceRange / 100} - £${this.state.topPriceRange / 100}`}</span>
            </div>

          </Row>

          <Row md={1}>
            {this.renderProduct()}
          </Row>
        </Row>

        <Row>
          {this.state.formattedMessages.join(' ')}
        </Row>

      </div>

    );
  }
}

function formatImageTag(imageSrcTemplate, shot, size) {
  return imageSrcTemplate
      .replace('{{scheme}}', '')
      .replace('{{shot}}', shot)
      .replace('{{size}}', size);
}

export default App;
