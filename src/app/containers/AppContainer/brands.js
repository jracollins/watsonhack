module.exports = [
  {
    id: 2346,
    name: '2XU',
    status: 'Visible',
    urlKey: '2XU'
  },
  {
    id: 473,
    name: 'A.P.C.',
    status: 'Visible',
    urlKey: 'APC'
  },
  {
    id: 1100,
    name: 'Abbeyhorn',
    status: 'Visible',
    urlKey: 'Abbeyhorn'
  },
  {
    id: 1480,
    name: 'Abrams',
    status: 'Visible',
    urlKey: 'Abrams'
  },
  {
    id: 404,
    name: 'Acne Studios',
    status: 'Visible',
    urlKey: 'Acne_Studios'
  },
  {
    id: 2411,
    name: 'adidas Consortium',
    status: 'ComingSoon',
    urlKey: 'adidas_Consortium'
  },
  {
    id: 1840,
    name: 'adidas Originals',
    status: 'Visible',
    urlKey: 'adidas_Originals'
  },
  {
    id: 1399,
    name: 'Aesop',
    status: 'Visible',
    urlKey: 'Aesop'
  },
  {
    id: 1712,
    name: 'AG Jeans',
    status: 'Visible',
    urlKey: 'AG_Jeans'
  },
  {
    id: 2487,
    name: 'Albam',
    status: 'Visible',
    urlKey: 'Albam'
  },
  {
    id: 250,
    name: 'Alexander McQueen',
    status: 'Visible',
    urlKey: 'Alexander_McQueen'
  },
  {
    id: 340,
    name: 'Alexander Wang',
    status: 'Visible',
    urlKey: 'Alexander_Wang'
  },
  {
    id: 1837,
    name: 'Alex Mill',
    status: 'Visible',
    urlKey: 'Alex_Mill'
  },
  {
    id: 1285,
    name: 'Alice Made This',
    status: 'Visible',
    urlKey: 'Alice_Made_This'
  },
  {
    id: 2434,
    name: 'Aloye',
    status: 'Visible',
    urlKey: 'Aloye'
  },
  {
    id: 1626,
    name: 'Álvaro',
    status: 'Visible',
    urlKey: 'Alvaro'
  },
  {
    id: 1081,
    name: 'AMI',
    status: 'Visible',
    urlKey: 'AMI'
  },
  {
    id: 1230,
    name: "Anderson's",
    status: 'Visible',
    urlKey: 'Andersons'
  },
  {
    id: 1858,
    name: 'Anonymous Ism',
    status: 'Visible',
    urlKey: 'Anonymous_Ism'
  },
  {
    id: 2344,
    name: "Arc'teryx",
    status: 'Visible',
    urlKey: 'Arcteryx'
  },
  {
    id: 1062,
    name: "Arc'teryx Veilance",
    status: 'Visible',
    urlKey: 'Arcteryx_Veilance'
  },
  {
    id: 2362,
    name: 'Arena',
    status: 'Visible',
    urlKey: 'Arena'
  },
  {
    id: 1607,
    name: 'Armand Diradourian',
    status: 'ComingSoon',
    urlKey: 'Armand_Diradourian'
  },
  {
    id: 1383,
    name: 'Armando Cabral',
    status: 'ComingSoon',
    urlKey: 'Armando_Cabral'
  },
  {
    id: 2584,
    name: 'ASICS',
    status: 'Visible',
    urlKey: 'ASICS'
  },
  {
    id: 1076,
    name: 'Aspesi',
    status: 'Visible',
    urlKey: 'Aspesi'
  },
  {
    id: 2757,
    name: 'Asprey',
    status: 'Visible',
    urlKey: 'Asprey'
  },
  {
    id: 1292,
    name: 'Assouline',
    status: 'Visible',
    urlKey: 'Assouline'
  },
  {
    id: 2181,
    name: 'Athletic Propulsion Labs',
    status: 'Visible',
    urlKey: 'Athletic_Propulsion_Labs'
  },
  {
    id: 2628,
    name: 'Aztech Mountain',
    status: 'Visible',
    urlKey: 'Aztech_Mountain'
  },
  {
    id: 1598,
    name: 'B&O Play',
    status: 'Visible',
    urlKey: 'BandO_Play'
  },
  {
    id: 592,
    name: 'Balenciaga',
    status: 'Visible',
    urlKey: 'Balenciaga'
  },
  {
    id: 472,
    name: 'Balmain',
    status: 'Visible',
    urlKey: 'Balmain'
  },
  {
    id: 2440,
    name: 'Bamford Grooming Department',
    status: 'Visible',
    urlKey: 'Bamford_Grooming_Department'
  },
  {
    id: 1852,
    name: 'Barena',
    status: 'Visible',
    urlKey: 'Barena'
  },
  {
    id: 1871,
    name: 'Barton Perreira',
    status: 'Visible',
    urlKey: 'Barton_Perreira'
  },
  {
    id: 2482,
    name: 'Battenwear',
    status: 'Visible',
    urlKey: 'Battenwear'
  },
  {
    id: 1646,
    name: 'Baxter of California',
    status: 'Visible',
    urlKey: 'Baxter_of_California'
  },
  {
    id: 2576,
    name: 'Beams',
    status: 'Visible',
    urlKey: 'Beams'
  },
  {
    id: 1257,
    name: 'Beams Plus',
    status: 'Visible',
    urlKey: 'Beams_Plus'
  },
  {
    id: 1976,
    name: 'Begg & Co',
    status: 'Visible',
    urlKey: 'Begg_and_Co'
  },
  {
    id: 584,
    name: 'Belstaff',
    status: 'Visible',
    urlKey: 'Belstaff'
  },
  {
    id: 1781,
    name: 'Berluti',
    status: 'Visible',
    urlKey: 'Berluti'
  },
  {
    id: 2513,
    name: 'Berry Bros. & Rudd Press',
    status: 'Visible',
    urlKey: 'Berry_Bros_and_Rudd_Press'
  },
  {
    id: 1739,
    name: 'Birkenstock',
    status: 'ComingSoon',
    urlKey: 'Birkenstock'
  },
  {
    id: 2330,
    name: 'Blackmeans',
    status: 'ComingSoon',
    urlKey: 'Blackmeans'
  },
  {
    id: 2238,
    name: 'Blind Barber',
    status: 'Visible',
    urlKey: 'Blind_Barber'
  },
  {
    id: 1663,
    name: 'Blue Blue Japan',
    status: 'Visible',
    urlKey: 'Blue_Blue_Japan'
  },
  {
    id: 2053,
    name: "Bodyism's Clean and Lean",
    status: 'Visible',
    urlKey: 'Bodyisms_Clean_and_Lean'
  },
  {
    id: 1208,
    name: 'Boglioli',
    status: 'Visible',
    urlKey: 'Boglioli'
  },
  {
    id: 2364,
    name: 'Bogner',
    status: 'Visible',
    urlKey: 'Bogner'
  },
  {
    id: 1358,
    name: 'Borsalino',
    status: 'ComingSoon',
    urlKey: 'Borsalino'
  },
  {
    id: 45,
    name: 'Bottega Veneta',
    status: 'Visible',
    urlKey: 'Bottega_Veneta'
  },
  {
    id: 1594,
    name: 'Bowers & Wilkins',
    status: 'Visible',
    urlKey: 'Bowers_and_Wilkins'
  },
  {
    id: 2575,
    name: 'Braun',
    status: 'Visible',
    urlKey: 'Braun'
  },
  {
    id: 1686,
    name: 'Bremont',
    status: 'Visible',
    urlKey: 'Bremont'
  },
  {
    id: 1103,
    name: 'Brioni',
    status: 'Visible',
    urlKey: 'Brioni'
  },
  {
    id: 1914,
    name: 'Brooks England',
    status: 'Visible',
    urlKey: 'Brooks_England'
  },
  {
    id: 2338,
    name: 'Brunello Cucinelli',
    status: 'Visible',
    urlKey: 'Brunello_Cucinelli'
  },
  {
    id: 2779,
    name: 'Buly 1803',
    status: 'Visible',
    urlKey: 'Buly_1803'
  },
  {
    id: 2619,
    name: 'Bumble and bumble',
    status: 'Visible',
    urlKey: 'Bumble_and_bumble'
  },
  {
    id: 2593,
    name: 'Burberry',
    status: 'Visible',
    urlKey: 'Burberry'
  },
  {
    id: 2090,
    name: 'BURTON',
    status: 'Visible',
    urlKey: 'BURTON'
  },
  {
    id: 1511,
    name: 'Byredo',
    status: 'Visible',
    urlKey: 'Byredo'
  },
  {
    id: 1760,
    name: 'By Walid',
    status: 'Visible',
    urlKey: 'By_Walid'
  },
  {
    id: 1973,
    name: 'C.O.Bigelow',
    status: 'ComingSoon',
    urlKey: 'COBigelow'
  },
  {
    id: 2118,
    name: 'Cafe du Cycliste',
    status: 'Visible',
    urlKey: 'Cafe_du_Cycliste'
  },
  {
    id: 161,
    name: 'Calvin Klein Collection',
    status: 'Visible',
    urlKey: 'Calvin_Klein_Collection'
  },
  {
    id: 465,
    name: 'Calvin Klein Underwear',
    status: 'Visible',
    urlKey: 'Calvin_Klein_Underwear'
  },
  {
    id: 2461,
    name: 'Camoshita',
    status: 'Visible',
    urlKey: 'Camoshita'
  },
  {
    id: 1075,
    name: 'Canada Goose',
    status: 'Visible',
    urlKey: 'Canada_Goose'
  },
  {
    id: 1134,
    name: 'Canali',
    status: 'Visible',
    urlKey: 'Canali'
  },
  {
    id: 1548,
    name: "Caran d'Ache",
    status: 'Visible',
    urlKey: 'Caran_dAche'
  },
  {
    id: 2460,
    name: 'Caruso',
    status: 'Visible',
    urlKey: 'Caruso'
  },
  {
    id: 447,
    name: 'Castañer',
    status: 'Visible',
    urlKey: 'Castaner'
  },
  {
    id: 2357,
    name: 'Castelli',
    status: 'Visible',
    urlKey: 'Castelli'
  },
  {
    id: 2419,
    name: 'Cav Empt',
    status: 'Visible',
    urlKey: 'Cav_Empt'
  },
  {
    id: 2632,
    name: 'Cedes Milano',
    status: 'Visible',
    urlKey: 'Cedes_Milano'
  },
  {
    id: 991,
    name: 'Chalayan',
    status: 'Visible',
    urlKey: 'Chalayan'
  },
  {
    id: 1082,
    name: 'Charvet',
    status: 'Visible',
    urlKey: 'Charvet'
  },
  {
    id: 1791,
    name: 'Cheaney',
    status: 'ComingSoon',
    urlKey: 'Cheaney'
  },
  {
    id: 2233,
    name: 'Chimala',
    status: 'Visible',
    urlKey: 'Chimala'
  },
  {
    id: 2516,
    name: 'Chpt.///',
    status: 'Visible',
    urlKey: 'chpt3'
  },
  {
    id: 510,
    name: 'Christopher Kane',
    status: 'Visible',
    urlKey: 'Christopher_Kane'
  },
  {
    id: 543,
    name: "Church's",
    status: 'Visible',
    urlKey: 'Churchs'
  },
  {
    id: 1301,
    name: 'Cire Trudon',
    status: 'Visible',
    urlKey: 'Cire_Trudon'
  },
  {
    id: 2753,
    name: 'Cleverly Laundry',
    status: 'Visible',
    urlKey: 'Cleverly_Laundry'
  },
  {
    id: 2509,
    name: 'Clinique For Men',
    status: 'Visible',
    urlKey: 'Clinique_For_Men'
  },
  {
    id: 2047,
    name: 'Club Monaco',
    status: 'Visible',
    urlKey: 'Club_Monaco'
  },
  {
    id: 2620,
    name: 'Colmar',
    status: 'Visible',
    urlKey: 'Colmar'
  },
  {
    id: 989,
    name: 'Comme des Garçons',
    status: 'Visible',
    urlKey: 'Comme_des_Garcons'
  },
  {
    id: 2502,
    name: 'Comme des Garcons Parfums',
    status: 'Visible',
    urlKey: 'Comme_des_Garcons_Parfums'
  },
  {
    id: 2450,
    name: 'Comme des Garçons SHIRT',
    status: 'ComingSoon',
    urlKey: 'Comme_des_Garcons_SHIRT'
  },
  {
    id: 1089,
    name: 'Common Projects',
    status: 'Visible',
    urlKey: 'Common_Projects'
  },
  {
    id: 2715,
    name: 'Connolly',
    status: 'Visible',
    urlKey: 'Connolly'
  },
  {
    id: 553,
    name: 'Converse',
    status: 'Visible',
    urlKey: 'Converse'
  },
  {
    id: 2640,
    name: 'Cordings',
    status: 'Visible',
    urlKey: 'Cordings'
  },
  {
    id: 1038,
    name: 'Corgi',
    status: 'Visible',
    urlKey: 'Corgi'
  },
  {
    id: 2273,
    name: 'COS',
    status: 'Visible',
    urlKey: 'COS'
  },
  {
    id: 506,
    name: 'Cutler and Gross',
    status: 'Visible',
    urlKey: 'Cutler_and_Gross'
  },
  {
    id: 1678,
    name: 'Czech & Speake',
    status: 'Visible',
    urlKey: 'Czech_and_Speake'
  },
  {
    id: 2280,
    name: 'D.S. & Durga',
    status: 'Visible',
    urlKey: 'DS_and_Durga'
  },
  {
    id: 2201,
    name: 'Deakin & Francis',
    status: 'Visible',
    urlKey: 'Deakin_and_Francis'
  },
  {
    id: 1130,
    name: 'Dents',
    status: 'Visible',
    urlKey: 'Dents'
  },
  {
    id: 954,
    name: 'Derek Rose',
    status: 'Visible',
    urlKey: 'Derek_Rose'
  },
  {
    id: 2300,
    name: 'Derm Ink',
    status: 'Visible',
    urlKey: 'Derm_Ink'
  },
  {
    id: 2358,
    name: 'Descente',
    status: 'Visible',
    urlKey: 'Descente'
  },
  {
    id: 2310,
    name: 'Diptyque',
    status: 'Visible',
    urlKey: 'Diptyque'
  },
  {
    id: 449,
    name: 'Dolce & Gabbana',
    status: 'Visible',
    urlKey: 'Dolce_and_Gabbana'
  },
  {
    id: 2029,
    name: 'Dr. Barbara Sturm',
    status: 'Visible',
    urlKey: 'Dr_Barbara_Sturm'
  },
  {
    id: 1707,
    name: "Dr. Jackson's",
    status: 'Visible',
    urlKey: 'Dr_Jacksons'
  },
  {
    id: 1024,
    name: "Drake's",
    status: 'Visible',
    urlKey: 'Drakes'
  },
  {
    id: 1679,
    name: 'D R Harris',
    status: 'Visible',
    urlKey: 'D_R_Harris'
  },
  {
    id: 643,
    name: 'Dries Van Noten',
    status: 'Visible',
    urlKey: 'Dries_Van_Noten'
  },
  {
    id: 1484,
    name: 'Dr Sebagh',
    status: 'Visible',
    urlKey: 'Dr_Sebagh'
  },
  {
    id: 950,
    name: 'Dunhill',
    status: 'Visible',
    urlKey: 'Dunhill'
  },
  {
    id: 2325,
    name: 'Eastpak',
    status: 'Visible',
    urlKey: 'Eastpak'
  },
  {
    id: 2221,
    name: 'Ebbets Field Flannels',
    status: 'Visible',
    urlKey: 'Ebbets_Field_Flannels'
  },
  {
    id: 2524,
    name: 'Ecco Golf',
    status: 'Visible',
    urlKey: 'Ecco_Golf'
  },
  {
    id: 1829,
    name: 'Edward Green',
    status: 'Visible',
    urlKey: 'Edward_Green'
  },
  {
    id: 2621,
    name: 'Eidos',
    status: 'ComingSoon',
    urlKey: 'Eidos'
  },
  {
    id: 1561,
    name: 'Emma Willis',
    status: 'Visible',
    urlKey: 'Emma_Willis'
  },
  {
    id: 2603,
    name: 'Engineered Garments',
    status: 'Visible',
    urlKey: 'Engineered_Garments'
  },
  {
    id: 2803,
    name: 'Enlist',
    status: 'Visible',
    urlKey: 'Enlist'
  },
  {
    id: 2571,
    name: 'Ermenegildo Zegna',
    status: 'Visible',
    urlKey: 'Ermenegildo_Zegna'
  },
  {
    id: 1617,
    name: 'Erno Laszlo',
    status: 'Visible',
    urlKey: 'Erno_Laszlo'
  },
  {
    id: 2318,
    name: 'Escentric Molecules',
    status: 'Visible',
    urlKey: 'Escentric_Molecules'
  },
  {
    id: 2227,
    name: 'Esdevium Games',
    status: 'Visible',
    urlKey: 'Esdevium_Games'
  },
  {
    id: 513,
    name: 'Etro',
    status: 'Visible',
    urlKey: 'Etro'
  },
  {
    id: 2389,
    name: 'Everest Isles',
    status: 'Visible',
    urlKey: 'Everest_Isles'
  },
  {
    id: 1846,
    name: 'Eyevan 7285',
    status: 'Visible',
    urlKey: 'Eyevan_7285'
  },
  {
    id: 2625,
    name: 'Fabbrica Pelletterie Milano',
    status: 'Visible',
    urlKey: 'Fabbrica_Pelletterie_Milano'
  },
  {
    id: 2377,
    name: 'Fabric-Brand & Co',
    status: 'Visible',
    urlKey: 'Fabric_Brand_and_Co'
  },
  {
    id: 2195,
    name: 'Faherty',
    status: 'Visible',
    urlKey: 'Faherty'
  },
  {
    id: 318,
    name: 'Falke',
    status: 'Visible',
    urlKey: 'Falke'
  },
  {
    id: 2369,
    name: 'Fanmail',
    status: 'Visible',
    urlKey: 'Fanmail'
  },
  {
    id: 2790,
    name: 'Farer',
    status: 'Visible',
    urlKey: 'Farer'
  },
  {
    id: 1596,
    name: 'Favourbrook',
    status: 'Visible',
    urlKey: 'Favourbrook'
  },
  {
    id: 2469,
    name: 'Feit',
    status: 'Visible',
    urlKey: 'Feit'
  },
  {
    id: 2730,
    name: 'Fellow Barber',
    status: 'Visible',
    urlKey: 'Fellow_Barber'
  },
  {
    id: 2366,
    name: 'Filling Pieces',
    status: 'Visible',
    urlKey: 'Filling_Pieces'
  },
  {
    id: 952,
    name: 'Filson',
    status: 'Visible',
    urlKey: 'Filson'
  },
  {
    id: 2363,
    name: 'Fizik',
    status: 'Visible',
    urlKey: 'Fizik'
  },
  {
    id: 2247,
    name: 'Floris London',
    status: 'Visible',
    urlKey: 'Floris_London'
  },
  {
    id: 1098,
    name: 'Folk',
    status: 'Visible',
    urlKey: 'Folk'
  },
  {
    id: 2190,
    name: 'Foreo',
    status: 'Visible',
    urlKey: 'Foreo'
  },
  {
    id: 1600,
    name: 'Fornasetti',
    status: 'Visible',
    urlKey: 'Fornasetti'
  },
  {
    id: 2802,
    name: 'Fortnum & Mason',
    status: 'Visible',
    urlKey: 'Fortnum_and_Mason'
  },
  {
    id: 1429,
    name: 'Foundwell Vintage',
    status: 'Visible',
    urlKey: 'Foundwell_Vintage'
  },
  {
    id: 1486,
    name: 'FRAME',
    status: 'Visible',
    urlKey: 'FRAME'
  },
  {
    id: 1716,
    name: 'Francesco Maglia',
    status: 'Visible',
    urlKey: 'Francesco_Maglia'
  },
  {
    id: 2661,
    name: 'Frederic Malle',
    status: 'Visible',
    urlKey: 'Frederic_Malle'
  },
  {
    id: 2119,
    name: 'Freemans Sporting Club',
    status: 'Visible',
    urlKey: 'Freemans_Sporting_Club'
  },
  {
    id: 1687,
    name: 'Frescobol Carioca',
    status: 'Visible',
    urlKey: 'Frescobol_Carioca'
  },
  {
    id: 1424,
    name: 'Garrett Leight California Optical',
    status: 'Visible',
    urlKey: 'Garrett_Leight_California_Optical'
  },
  {
    id: 2731,
    name: 'Gaziano & Girling',
    status: 'Visible',
    urlKey: 'Gaziano_and_Girling'
  },
  {
    id: 1556,
    name: 'George Cleverley',
    status: 'Visible',
    urlKey: 'George_Cleverley'
  },
  {
    id: 2008,
    name: 'Gieves & Hawkes',
    status: 'Visible',
    urlKey: 'Gieves_and_Hawkes'
  },
  {
    id: 1315,
    name: 'Giorgio Armani',
    status: 'Visible',
    urlKey: 'Giorgio_Armani'
  },
  {
    id: 1114,
    name: 'Gitman Vintage',
    status: 'Visible',
    urlKey: 'Gitman_Vintage'
  },
  {
    id: 406,
    name: 'Giuseppe Zanotti',
    status: 'Visible',
    urlKey: 'Giuseppe_Zanotti'
  },
  {
    id: 170,
    name: 'Givenchy',
    status: 'Visible',
    urlKey: 'Givenchy'
  },
  {
    id: 1565,
    name: 'Globe-Trotter',
    status: 'Visible',
    urlKey: 'Globe_Trotter'
  },
  {
    id: 2393,
    name: 'Golden Goose Deluxe Brand',
    status: 'Visible',
    urlKey: 'Golden_Goose_Deluxe_Brand'
  },
  {
    id: 2799,
    name: 'GoPro',
    status: 'Visible',
    urlKey: 'GoPro'
  },
  {
    id: 2368,
    name: 'GREATS',
    status: 'Visible',
    urlKey: 'GREATS'
  },
  {
    id: 1187,
    name: 'Grenson',
    status: 'Visible',
    urlKey: 'Grenson'
  },
  {
    id: 578,
    name: 'Gucci',
    status: 'Visible',
    urlKey: 'Gucci'
  },
  {
    id: 1681,
    name: 'Guidi',
    status: 'Visible',
    urlKey: 'Guidi'
  },
  {
    id: 2717,
    name: 'GW Scott',
    status: 'Visible',
    urlKey: 'GW_Scott'
  },
  {
    id: 1586,
    name: 'Hackett',
    status: 'Visible',
    urlKey: 'Hackett'
  },
  {
    id: 1390,
    name: 'Haider Ackermann',
    status: 'Visible',
    urlKey: 'Haider_Ackermann'
  },
  {
    id: 1453,
    name: 'Hampton Sun',
    status: 'Visible',
    urlKey: 'Hampton_Sun'
  },
  {
    id: 2296,
    name: 'Handvaerk',
    status: 'Visible',
    urlKey: 'Handvaerk'
  },
  {
    id: 1590,
    name: 'Hanro',
    status: 'Visible',
    urlKey: 'Hanro'
  },
  {
    id: 1589,
    name: 'Hardy Amies',
    status: 'Visible',
    urlKey: 'Hardy_Amies'
  },
  {
    id: 1069,
    name: 'Harrys of London',
    status: 'Visible',
    urlKey: 'Harrys_of_London'
  },
  {
    id: 1011,
    name: 'Hartford',
    status: 'Visible',
    urlKey: 'Hartford'
  },
  {
    id: 744,
    name: 'Havaianas',
    status: 'Visible',
    urlKey: 'Havaianas'
  },
  {
    id: 2493,
    name: 'Hector Saxe',
    status: 'Visible',
    urlKey: 'Hector_Saxe'
  },
  {
    id: 2601,
    name: 'Helbers',
    status: 'Visible',
    urlKey: 'Helbers'
  },
  {
    id: 2359,
    name: 'Hender Scheme',
    status: 'Visible',
    urlKey: 'Hender_Scheme'
  },
  {
    id: 2616,
    name: 'Herno Laminar',
    status: 'Visible',
    urlKey: 'Herno_Laminar'
  },
  {
    id: 1583,
    name: 'Heschung',
    status: 'Visible',
    urlKey: 'Heschung'
  },
  {
    id: 2380,
    name: 'Hestra',
    status: 'Visible',
    urlKey: 'Hestra'
  },
  {
    id: 2804,
    name: 'Hopper',
    status: 'Visible',
    urlKey: 'Hopper'
  },
  {
    id: 2613,
    name: "Howlin'",
    status: 'Visible',
    urlKey: 'Howlin'
  },
  {
    id: 2332,
    name: 'Hugo Boss',
    status: 'Visible',
    urlKey: 'Hugo_Boss'
  },
  {
    id: 358,
    name: 'Hunter Original',
    status: 'Visible',
    urlKey: 'Hunter_Original'
  },
  {
    id: 1802,
    name: 'Iffley Road',
    status: 'Visible',
    urlKey: 'Iffley_Road'
  },
  {
    id: 1209,
    name: 'Illesteva',
    status: 'Visible',
    urlKey: 'Illesteva'
  },
  {
    id: 2668,
    name: 'impossible Project',
    status: 'Visible',
    urlKey: 'impossible_Project'
  },
  {
    id: 1417,
    name: 'Incotex',
    status: 'Visible',
    urlKey: 'Incotex'
  },
  {
    id: 2726,
    name: 'Inis Meáin',
    status: 'Visible',
    urlKey: 'Inis_Meain'
  },
  {
    id: 2605,
    name: 'Isabel Benenato',
    status: 'Visible',
    urlKey: 'Isabel_Benenato'
  },
  {
    id: 2016,
    name: 'Isaia',
    status: 'Visible',
    urlKey: 'Isaia'
  },
  {
    id: 2056,
    name: 'Issey Miyake Men',
    status: 'Visible',
    urlKey: 'Issey_Miyake_Men'
  },
  {
    id: 2829,
    name: 'IWC SCHAFFHAUSEN',
    status: 'Visible',
    urlKey: 'IWC_SCHAFFHAUSEN'
  },
  {
    id: 870,
    name: 'J.Crew',
    status: 'Visible',
    urlKey: 'JCrew'
  },
  {
    id: 2025,
    name: 'J.M. Weston',
    status: 'Visible',
    urlKey: 'JM_Weston'
  },
  {
    id: 1164,
    name: 'J.W.Anderson',
    status: 'Visible',
    urlKey: 'JWAnderson'
  },
  {
    id: 392,
    name: 'James Perse',
    status: 'Visible',
    urlKey: 'James_Perse'
  },
  {
    id: 2480,
    name: 'James Tanner',
    status: 'ComingSoon',
    urlKey: 'James_Tanner'
  },
  {
    id: 2703,
    name: 'Japan Best',
    status: 'Visible',
    urlKey: 'Japan_Best'
  },
  {
    id: 2462,
    name: 'Jason Markk',
    status: 'Visible',
    urlKey: 'Jason_Markk'
  },
  {
    id: 967,
    name: 'Jean Shop',
    status: 'Visible',
    urlKey: 'Jean_Shop'
  },
  {
    id: 523,
    name: 'Jil Sander',
    status: 'Visible',
    urlKey: 'Jil_Sander'
  },
  {
    id: 4,
    name: 'Jimmy Choo',
    status: 'Visible',
    urlKey: 'Jimmy_Choo'
  },
  {
    id: 2484,
    name: 'John Elliott',
    status: 'Visible',
    urlKey: 'John_Elliott'
  },
  {
    id: 953,
    name: 'John Lobb',
    status: 'Visible',
    urlKey: 'John_Lobb'
  },
  {
    id: 974,
    name: 'John Smedley',
    status: 'Visible',
    urlKey: 'John_Smedley'
  },
  {
    id: 2205,
    name: 'Junghans',
    status: 'Visible',
    urlKey: 'Junghans'
  },
  {
    id: 1005,
    name: 'Junya Watanabe',
    status: 'Visible',
    urlKey: 'Junya_Watanabe'
  },
  {
    id: 2413,
    name: 'KAPITAL',
    status: 'Visible',
    urlKey: 'KAPITAL'
  },
  {
    id: 2742,
    name: 'Kent & Curwen',
    status: 'Visible',
    urlKey: 'Kent_and_Curwen'
  },
  {
    id: 1838,
    name: 'Kingsman',
    status: 'Visible',
    urlKey: 'Kingsman'
  },
  {
    id: 1372,
    name: 'Kjus',
    status: 'Visible',
    urlKey: 'Kjus'
  },
  {
    id: 2618,
    name: 'Kjus Golf',
    status: 'Visible',
    urlKey: 'Kjus_Golf'
  },
  {
    id: 1698,
    name: 'L.G.R',
    status: 'Visible',
    urlKey: 'LGR'
  },
  {
    id: 2553,
    name: 'Lab Series',
    status: 'Visible',
    urlKey: 'Lab_Series'
  },
  {
    id: 2129,
    name: 'Lacoste',
    status: 'Visible',
    urlKey: 'Lacoste'
  },
  {
    id: 2130,
    name: 'Lacoste Tennis',
    status: 'Visible',
    urlKey: 'Lacoste_Tennis'
  },
  {
    id: 2475,
    name: 'La Mer',
    status: 'Visible',
    urlKey: 'La_Mer'
  },
  {
    id: 502,
    name: 'Lanvin',
    status: 'Visible',
    urlKey: 'Lanvin'
  },
  {
    id: 2225,
    name: 'Larose',
    status: 'ComingSoon',
    urlKey: 'Larose'
  },
  {
    id: 1710,
    name: 'Lavett & Chin',
    status: 'Visible',
    urlKey: 'Lavett_and_Chin'
  },
  {
    id: 1715,
    name: 'Le Gramme',
    status: 'Visible',
    urlKey: 'Le_Gramme'
  },
  {
    id: 2421,
    name: 'Leica',
    status: 'Visible',
    urlKey: 'Leica'
  },
  {
    id: 1680,
    name: 'Le Labo',
    status: 'Visible',
    urlKey: 'Le_Labo'
  },
  {
    id: 955,
    name: "Levi's",
    status: 'Visible',
    urlKey: 'Levis'
  },
  {
    id: 1006,
    name: "Levi's Made & Crafted",
    status: 'Visible',
    urlKey: 'Levis_Made_and_Crafted'
  },
  {
    id: 1048,
    name: "Levi's Vintage Clothing",
    status: 'Visible',
    urlKey: 'Levis_Vintage_Clothing'
  },
  {
    id: 2638,
    name: 'Linley',
    status: 'Visible',
    urlKey: 'Linley'
  },
  {
    id: 975,
    name: 'Lock & Co Hatters',
    status: 'Visible',
    urlKey: 'Lock_and_Co_Hatters'
  },
  {
    id: 1532,
    name: 'Loewe',
    status: 'Visible',
    urlKey: 'Loewe'
  },
  {
    id: 1288,
    name: 'London Undercover',
    status: 'Visible',
    urlKey: 'London_Undercover'
  },
  {
    id: 1097,
    name: 'Loro Piana',
    status: 'Visible',
    urlKey: 'Loro_Piana'
  },
  {
    id: 1041,
    name: 'Luis Morais',
    status: 'Visible',
    urlKey: 'Luis_Morais'
  },
  {
    id: 1273,
    name: 'Luminox',
    status: 'Visible',
    urlKey: 'Luminox'
  },
  {
    id: 2229,
    name: 'M.E. Skin Lab',
    status: 'Visible',
    urlKey: 'ME_Skin_Lab'
  },
  {
    id: 1144,
    name: 'Mackintosh',
    status: 'Visible',
    urlKey: 'Mackintosh'
  },
  {
    id: 2244,
    name: 'Maison Francis Kurkdjian',
    status: 'Visible',
    urlKey: 'Maison_Francis_Kurkdjian'
  },
  {
    id: 1084,
    name: 'Maison Kitsuné',
    status: 'Visible',
    urlKey: 'Maison_Kitsune'
  },
  {
    id: 427,
    name: 'Maison Margiela',
    status: 'Visible',
    urlKey: 'Maison_Margiela'
  },
  {
    id: 1676,
    name: 'Malin + Goetz',
    status: 'Visible',
    urlKey: 'Malin_and_Goetz'
  },
  {
    id: 115,
    name: 'Marc Jacobs',
    status: 'Visible',
    urlKey: 'Marc_Jacobs'
  },
  {
    id: 2481,
    name: 'Marcoliani',
    status: 'Visible',
    urlKey: 'Marcoliani'
  },
  {
    id: 959,
    name: 'Margaret Howell',
    status: 'Visible',
    urlKey: 'Margaret_Howell'
  },
  {
    id: 128,
    name: 'Marni',
    status: 'Visible',
    urlKey: 'Marni'
  },
  {
    id: 1473,
    name: 'Marsell',
    status: 'Visible',
    urlKey: 'Marsell'
  },
  {
    id: 1545,
    name: 'Marvis',
    status: 'Visible',
    urlKey: 'Marvis'
  },
  {
    id: 2418,
    name: 'Marvy Jamoke',
    status: 'Visible',
    urlKey: 'Marvy_Jamoke'
  },
  {
    id: 1357,
    name: 'Marwood',
    status: 'Visible',
    urlKey: 'Marwood'
  },
  {
    id: 1503,
    name: 'Massimo Alba',
    status: 'Visible',
    urlKey: 'Massimo_Alba'
  },
  {
    id: 2202,
    name: 'Master & Dynamic',
    status: 'Visible',
    urlKey: 'Master_and_Dynamic'
  },
  {
    id: 1576,
    name: 'Master-Piece',
    status: 'Visible',
    urlKey: 'Master_Piece'
  },
  {
    id: 2361,
    name: 'Matuse',
    status: 'Visible',
    urlKey: 'Matuse'
  },
  {
    id: 2778,
    name: 'Max Pittion',
    status: 'Visible',
    urlKey: 'Max_Pittion'
  },
  {
    id: 2729,
    name: 'McCaffrey',
    status: 'Visible',
    urlKey: 'McCaffrey'
  },
  {
    id: 313,
    name: 'McQ Alexander McQueen',
    status: 'Visible',
    urlKey: 'McQ_Alexander_McQueen'
  },
  {
    id: 1239,
    name: 'Miansai',
    status: 'Visible',
    urlKey: 'Miansai'
  },
  {
    id: 1205,
    name: 'Michael Bastian',
    status: 'Visible',
    urlKey: 'Michael_Bastian'
  },
  {
    id: 140,
    name: 'Michael Kors',
    status: 'Visible',
    urlKey: 'Michael_Kors'
  },
  {
    id: 2365,
    name: 'Mikia',
    status: 'ComingSoon',
    urlKey: 'Mikia'
  },
  {
    id: 78,
    name: 'Missoni',
    status: 'Visible',
    urlKey: 'Missoni'
  },
  {
    id: 2110,
    name: 'Mollusk',
    status: 'Visible',
    urlKey: 'Mollusk'
  },
  {
    id: 2091,
    name: 'Moncler',
    status: 'Visible',
    urlKey: 'Moncler'
  },
  {
    id: 2609,
    name: 'Moncler Gamme Bleu',
    status: 'Visible',
    urlKey: 'Moncler_Gamme_Bleu'
  },
  {
    id: 2373,
    name: 'Moncler Grenoble',
    status: 'Visible',
    urlKey: 'Moncler_Grenoble'
  },
  {
    id: 2002,
    name: 'Mondaine',
    status: 'Visible',
    urlKey: 'Mondaine'
  },
  {
    id: 1709,
    name: 'Montblanc',
    status: 'Visible',
    urlKey: 'Montblanc'
  },
  {
    id: 2503,
    name: 'Moscot',
    status: 'Visible',
    urlKey: 'Moscot'
  },
  {
    id: 1472,
    name: 'MP Massimo Piombo',
    status: 'ComingSoon',
    urlKey: 'MP_Massimo_Piombo'
  },
  {
    id: 2301,
    name: 'Mr. Gray',
    status: 'Visible',
    urlKey: 'Mr_Gray'
  },
  {
    id: 2488,
    name: 'MR PORTER GROOMING',
    status: 'Visible',
    urlKey: 'MR_PORTER_GROOMING'
  },
  {
    id: 223,
    name: 'Mulberry',
    status: 'Visible',
    urlKey: 'Mulberry'
  },
  {
    id: 2176,
    name: 'Mulo',
    status: 'Visible',
    urlKey: 'Mulo'
  },
  {
    id: 1261,
    name: 'Musto Sailing',
    status: 'Visible',
    urlKey: 'Musto_Sailing'
  },
  {
    id: 1422,
    name: 'Musto Shooting',
    status: 'Visible',
    urlKey: 'Musto_Shooting'
  },
  {
    id: 2266,
    name: 'Native Union',
    status: 'Visible',
    urlKey: 'Native_Union'
  },
  {
    id: 2808,
    name: 'Neat Nutrition',
    status: 'Visible',
    urlKey: 'Neat_Nutrition'
  },
  {
    id: 1393,
    name: 'Neighborhood',
    status: 'Visible',
    urlKey: 'Neighborhood'
  },
  {
    id: 1186,
    name: 'Neil Barrett',
    status: 'Visible',
    urlKey: 'Neil_Barrett'
  },
  {
    id: 1212,
    name: 'New Balance',
    status: 'Visible',
    urlKey: 'New_Balance'
  },
  {
    id: 2348,
    name: 'Newton',
    status: 'Visible',
    urlKey: 'Newton'
  },
  {
    id: 2617,
    name: 'Nigel Cabourn',
    status: 'Visible',
    urlKey: 'Nigel_Cabourn'
  },
  {
    id: 1051,
    name: 'Nike',
    status: 'Visible',
    urlKey: 'Nike'
  },
  {
    id: 2529,
    name: 'Nike Golf',
    status: 'Visible',
    urlKey: 'Nike_Golf'
  },
  {
    id: 2406,
    name: 'Nike Running',
    status: 'Visible',
    urlKey: 'Nike_Running'
  },
  {
    id: 2407,
    name: 'Nike Tennis',
    status: 'Visible',
    urlKey: 'Nike_Tennis'
  },
  {
    id: 2408,
    name: 'Nike Training',
    status: 'Visible',
    urlKey: 'Nike_Training'
  },
  {
    id: 1388,
    name: 'Nike x Undercover',
    status: 'ComingSoon',
    urlKey: 'Nike_x_Undercover'
  },
  {
    id: 1361,
    name: 'NN07',
    status: 'Visible',
    urlKey: 'NN07'
  },
  {
    id: 2552,
    name: 'nonnative',
    status: 'Visible',
    urlKey: 'nonnative'
  },
  {
    id: 1224,
    name: 'Nudie Jeans',
    status: 'Visible',
    urlKey: 'Nudie_Jeans'
  },
  {
    id: 1241,
    name: "O'Keeffe",
    status: 'Visible',
    urlKey: 'OKeeffe'
  },
  {
    id: 1979,
    name: 'Oakley',
    status: 'Visible',
    urlKey: 'Oakley'
  },
  {
    id: 2084,
    name: 'OAMC',
    status: 'Visible',
    urlKey: 'OAMC'
  },
  {
    id: 2120,
    name: 'Odin New York',
    status: 'Visible',
    urlKey: 'Odin_New_York'
  },
  {
    id: 1575,
    name: 'Officine Creative',
    status: 'Visible',
    urlKey: 'Officine_Creative'
  },
  {
    id: 1660,
    name: 'Officine Generale',
    status: 'Visible',
    urlKey: 'Officine_Generale'
  },
  {
    id: 421,
    name: 'Oliver Peoples',
    status: 'Visible',
    urlKey: 'Oliver_Peoples'
  },
  {
    id: 1099,
    name: 'Oliver Spencer',
    status: 'Visible',
    urlKey: 'Oliver_Spencer'
  },
  {
    id: 2284,
    name: 'Oliver Spencer Loungewear',
    status: 'Visible',
    urlKey: 'Oliver_Spencer_Loungewear'
  },
  {
    id: 2428,
    name: 'Onia',
    status: 'Visible',
    urlKey: 'Onia'
  },
  {
    id: 1441,
    name: 'Oribe',
    status: 'Visible',
    urlKey: 'Oribe'
  },
  {
    id: 2718,
    name: 'Oris',
    status: 'Visible',
    urlKey: 'Oris'
  },
  {
    id: 960,
    name: 'Orlebar Brown',
    status: 'Visible',
    urlKey: 'Orlebar_Brown'
  },
  {
    id: 2478,
    name: 'OrSlow',
    status: 'Visible',
    urlKey: 'OrSlow'
  },
  {
    id: 1203,
    name: 'Our Legacy',
    status: 'Visible',
    urlKey: 'Our_Legacy'
  },
  {
    id: 2376,
    name: 'Outerknown',
    status: 'Visible',
    urlKey: 'Outerknown'
  },
  {
    id: 2185,
    name: 'Pankhurst London',
    status: 'Visible',
    urlKey: 'Pankhurst_London'
  },
  {
    id: 1023,
    name: 'Pantherella',
    status: 'Visible',
    urlKey: 'Pantherella'
  },
  {
    id: 2424,
    name: 'Pas Normal Studios',
    status: 'Visible',
    urlKey: 'Pas_Normal_Studios'
  },
  {
    id: 1782,
    name: 'Patagonia',
    status: 'Visible',
    urlKey: 'Patagonia'
  },
  {
    id: 2197,
    name: 'Patricks',
    status: 'Visible',
    urlKey: 'Patricks'
  },
  {
    id: 109,
    name: 'Paul Smith',
    status: 'Visible',
    urlKey: 'Paul_Smith'
  },
  {
    id: 1574,
    name: 'Peak Performance',
    status: 'Visible',
    urlKey: 'Peak_Performance'
  },
  {
    id: 2175,
    name: "Penhaligon's",
    status: 'Visible',
    urlKey: 'Penhaligons'
  },
  {
    id: 1706,
    name: 'Perricone MD',
    status: 'Visible',
    urlKey: 'Perricone_MD'
  },
  {
    id: 591,
    name: 'Persol',
    status: 'Visible',
    urlKey: 'Persol'
  },
  {
    id: 2470,
    name: 'Peyote Bird',
    status: 'Visible',
    urlKey: 'Peyote_Bird'
  },
  {
    id: 2241,
    name: 'Phaidon',
    status: 'Visible',
    urlKey: 'Phaidon'
  },
  {
    id: 2353,
    name: 'Phenix',
    status: 'Visible',
    urlKey: 'Phenix'
  },
  {
    id: 2356,
    name: 'POC',
    status: 'Visible',
    urlKey: 'POC'
  },
  {
    id: 990,
    name: 'Polo Ralph Lauren',
    status: 'Visible',
    urlKey: 'Polo_Ralph_Lauren'
  },
  {
    id: 450,
    name: 'Prada',
    status: 'Visible',
    urlKey: 'Prada'
  },
  {
    id: 1373,
    name: 'Private White V.C.',
    status: 'Visible',
    urlKey: 'Private_White_VC'
  },
  {
    id: 962,
    name: 'PS by Paul Smith',
    status: 'Visible',
    urlKey: 'PS_by_Paul_Smith'
  },
  {
    id: 1825,
    name: 'Public School',
    status: 'Visible',
    urlKey: 'Public_School'
  },
  {
    id: 963,
    name: 'Quoddy',
    status: 'Visible',
    urlKey: 'Quoddy'
  },
  {
    id: 2127,
    name: 'R.M.Williams',
    status: 'Visible',
    urlKey: 'RMWilliams'
  },
  {
    id: 2766,
    name: 'Raden',
    status: 'Visible',
    urlKey: 'Raden'
  },
  {
    id: 1118,
    name: 'Raf Simons',
    status: 'Visible',
    urlKey: 'Raf_Simons'
  },
  {
    id: 860,
    name: 'rag & bone',
    status: 'Visible',
    urlKey: 'rag_and_bone'
  },
  {
    id: 1020,
    name: 'Ralph Lauren Purple Label',
    status: 'Visible',
    urlKey: 'Ralph_Lauren_Purple_Label'
  },
  {
    id: 451,
    name: 'Ray-Ban',
    status: 'Visible',
    urlKey: 'RayBan'
  },
  {
    id: 1359,
    name: 'Red Wing Shoes',
    status: 'Visible',
    urlKey: 'Red_Wing_Shoes'
  },
  {
    id: 2307,
    name: 'Reigning Champ',
    status: 'Visible',
    urlKey: 'Reigning_Champ'
  },
  {
    id: 1743,
    name: 'Remi Relief',
    status: 'Visible',
    urlKey: 'Remi_Relief'
  },
  {
    id: 2520,
    name: 'Renessence',
    status: 'Visible',
    urlKey: 'Renessence'
  },
  {
    id: 1763,
    name: 'REN Skincare',
    status: 'Visible',
    urlKey: 'REN_Skincare'
  },
  {
    id: 2494,
    name: 'Ressence',
    status: 'Visible',
    urlKey: 'Ressence'
  },
  {
    id: 973,
    name: 'Richard James',
    status: 'Visible',
    urlKey: 'Richard_James'
  },
  {
    id: 142,
    name: 'Rick Owens',
    status: 'Visible',
    urlKey: 'Rick_Owens'
  },
  {
    id: 1562,
    name: 'Rimowa',
    status: 'Visible',
    urlKey: 'Rimowa'
  },
  {
    id: 964,
    name: 'Rivieras',
    status: 'Visible',
    urlKey: 'Rivieras'
  },
  {
    id: 2182,
    name: 'RLX Ralph Lauren',
    status: 'ComingSoon',
    urlKey: 'RLX_Ralph_Lauren'
  },
  {
    id: 2345,
    name: 'Rocky Mountain Featherbed',
    status: 'Visible',
    urlKey: 'Rocky_Mountain_Featherbed'
  },
  {
    id: 2278,
    name: 'RRL',
    status: 'Visible',
    urlKey: 'RRL'
  },
  {
    id: 2351,
    name: 'Rubinacci',
    status: 'Visible',
    urlKey: 'Rubinacci'
  },
  {
    id: 1661,
    name: 'Sacai',
    status: 'Visible',
    urlKey: 'Sacai'
  },
  {
    id: 1777,
    name: 'SACHAJUAN',
    status: 'Visible',
    urlKey: 'SACHAJUAN'
  },
  {
    id: 2810,
    name: 'Sail Racing',
    status: 'Visible',
    urlKey: 'Sail_Racing'
  },
  {
    id: 1442,
    name: 'Saint Laurent',
    status: 'Visible',
    urlKey: 'Saint_Laurent'
  },
  {
    id: 2630,
    name: 'Salomon',
    status: 'Visible',
    urlKey: 'Salomon'
  },
  {
    id: 882,
    name: 'Sandro',
    status: 'Visible',
    urlKey: 'Sandro'
  },
  {
    id: 1703,
    name: 'Santa Maria Novella',
    status: 'Visible',
    urlKey: 'Santa_Maria_Novella'
  },
  {
    id: 1380,
    name: 'Santiago Gonzalez',
    status: 'Visible',
    urlKey: 'Santiago_Gonzalez'
  },
  {
    id: 2114,
    name: 'Santoni',
    status: 'Visible',
    urlKey: 'Santoni'
  },
  {
    id: 2476,
    name: 'Sasquatchfabrix.',
    status: 'Visible',
    urlKey: 'Sasquatchfabrix'
  },
  {
    id: 1168,
    name: 'Saturdays NYC',
    status: 'Visible',
    urlKey: 'Saturdays_NYC'
  },
  {
    id: 1384,
    name: 'Schiesser',
    status: 'Visible',
    urlKey: 'Schiesser'
  },
  {
    id: 2448,
    name: 'Sekford',
    status: 'Visible',
    urlKey: 'Sekford'
  },
  {
    id: 2374,
    name: 'Several',
    status: 'Visible',
    urlKey: 'Several'
  },
  {
    id: 2009,
    name: 'Shinola',
    status: 'Visible',
    urlKey: 'Shinola'
  },
  {
    id: 2704,
    name: 'Sidi',
    status: 'Visible',
    urlKey: 'Sidi'
  },
  {
    id: 1501,
    name: 'Simon Miller',
    status: 'Visible',
    urlKey: 'Simon_Miller'
  },
  {
    id: 2117,
    name: 'Sleepy Jones',
    status: 'Visible',
    urlKey: 'Sleepy_Jones'
  },
  {
    id: 956,
    name: 'Slowear',
    status: 'Visible',
    urlKey: 'Slowear'
  },
  {
    id: 598,
    name: 'Smythson',
    status: 'Visible',
    urlKey: 'Smythson'
  },
  {
    id: 2372,
    name: 'Snow Peak',
    status: 'Visible',
    urlKey: 'Snow_Peak'
  },
  {
    id: 2507,
    name: 'Soar Running',
    status: 'Visible',
    urlKey: 'Soar_Running'
  },
  {
    id: 2812,
    name: 'Soho Home',
    status: 'Visible',
    urlKey: 'Soho_Home'
  },
  {
    id: 2657,
    name: 'Solid Homme',
    status: 'Visible',
    urlKey: 'Solid_Homme'
  },
  {
    id: 2243,
    name: 'Sonic Editions',
    status: 'Visible',
    urlKey: 'Sonic_Editions'
  },
  {
    id: 1891,
    name: 'Sorel',
    status: 'Visible',
    urlKey: 'Sorel'
  },
  {
    id: 2765,
    name: 'Spalwart',
    status: 'ComingSoon',
    urlKey: 'Spalwart'
  },
  {
    id: 1414,
    name: 'Sperry Top-Sider',
    status: 'Visible',
    urlKey: 'Sperry_Top_Sider'
  },
  {
    id: 290,
    name: 'Stella McCartney',
    status: 'Visible',
    urlKey: 'Stella_McCartney'
  },
  {
    id: 512,
    name: 'Steven Alan',
    status: 'Visible',
    urlKey: 'Steven_Alan'
  },
  {
    id: 1499,
    name: 'Stone Island',
    status: 'Visible',
    urlKey: 'Stone_Island'
  },
  {
    id: 2375,
    name: 'Stüssy',
    status: 'Visible',
    urlKey: 'Stussy'
  },
  {
    id: 2126,
    name: 'Suicoke',
    status: 'ComingSoon',
    urlKey: 'Suicoke'
  },
  {
    id: 1571,
    name: 'Sulka',
    status: 'Visible',
    urlKey: 'Sulka'
  },
  {
    id: 1025,
    name: 'Sundek',
    status: 'Visible',
    urlKey: 'Sundek'
  },
  {
    id: 958,
    name: 'Sunspel',
    status: 'Visible',
    urlKey: 'Sunspel'
  },
  {
    id: 2371,
    name: 'Suunto',
    status: 'Visible',
    urlKey: 'Suunto'
  },
  {
    id: 1387,
    name: 'SWIMS',
    status: 'Visible',
    urlKey: 'SWIMS'
  },
  {
    id: 2602,
    name: 'TAKAHIROMIYASHITA TheSoloist.',
    status: 'Visible',
    urlKey: 'TAKAHIROMIYASHITA_TheSoloist'
  },
  {
    id: 2200,
    name: 'Tarnsjo Garveri',
    status: 'Visible',
    urlKey: 'Tarnsjo_Garveri'
  },
  {
    id: 1367,
    name: 'Taschen',
    status: 'Visible',
    urlKey: 'Taschen'
  },
  {
    id: 2396,
    name: 'TATEOSSIAN',
    status: 'Visible',
    urlKey: 'TATEOSSIAN'
  },
  {
    id: 2291,
    name: 'Teva',
    status: 'Visible',
    urlKey: 'Teva'
  },
  {
    id: 1119,
    name: 'The Elder Statesman',
    status: 'Visible',
    urlKey: 'The_Elder_Statesman'
  },
  {
    id: 1674,
    name: 'The Laundress',
    status: 'Visible',
    urlKey: 'The_Laundress'
  },
  {
    id: 1551,
    name: 'The Mr Porter Paperback',
    status: 'Visible',
    urlKey: 'The_Mr_Porter_Paperback'
  },
  {
    id: 537,
    name: 'Theory',
    status: 'Visible',
    urlKey: 'Theory'
  },
  {
    id: 2791,
    name: "The Perfumer's Story by Azzi Glasser",
    status: 'Visible',
    urlKey: 'The_Perfumers_Story_by_Azzi_Glasser'
  },
  {
    id: 2395,
    name: 'The Workers Club',
    status: 'Visible',
    urlKey: 'The_Workers_Club'
  },
  {
    id: 2637,
    name: 'This Is Ground',
    status: 'Visible',
    urlKey: 'This_Is_Ground'
  },
  {
    id: 1349,
    name: 'Thom Browne',
    status: 'Visible',
    urlKey: 'Thom_Browne'
  },
  {
    id: 1671,
    name: 'Thom Sweeney',
    status: 'Visible',
    urlKey: 'Thom_Sweeney'
  },
  {
    id: 2500,
    name: 'Thorogood',
    status: 'Visible',
    urlKey: 'Thorogood'
  },
  {
    id: 2665,
    name: 'Thorsun',
    status: 'Visible',
    urlKey: 'Thorsun'
  },
  {
    id: 2381,
    name: 'Timberland',
    status: 'Visible',
    urlKey: 'Timberland'
  },
  {
    id: 2725,
    name: 'Timex',
    status: 'Visible',
    urlKey: 'Timex'
  },
  {
    id: 398,
    name: "Tod's",
    status: 'Visible',
    urlKey: 'Tods'
  },
  {
    id: 1502,
    name: 'Todd Snyder',
    status: 'Visible',
    urlKey: 'Todd_Snyder'
  },
  {
    id: 2486,
    name: 'Todd Snyder + Champion',
    status: 'ComingSoon',
    urlKey: 'Todd_Snyder_and_Champion'
  },
  {
    id: 101,
    name: 'Tomas Maier',
    status: 'Visible',
    urlKey: 'Tomas_Maier'
  },
  {
    id: 2246,
    name: 'Tom Daxon',
    status: 'Visible',
    urlKey: 'Tom_Daxon'
  },
  {
    id: 1506,
    name: 'Tom Dixon',
    status: 'Visible',
    urlKey: 'Tom_Dixon'
  },
  {
    id: 424,
    name: 'TOM FORD',
    status: 'Visible',
    urlKey: 'TOM_FORD'
  },
  {
    id: 2431,
    name: 'Tom Ford Beauty',
    status: 'Visible',
    urlKey: 'Tom_Ford_Beauty'
  },
  {
    id: 1826,
    name: 'Tomorrowland',
    status: 'ComingSoon',
    urlKey: 'Tomorrowland'
  },
  {
    id: 2832,
    name: 'Tracksmith',
    status: 'Visible',
    urlKey: 'Tracksmith'
  },
  {
    id: 1736,
    name: 'Trianon',
    status: 'Visible',
    urlKey: 'Trianon'
  },
  {
    id: 2627,
    name: "Tricker's",
    status: 'Visible',
    urlKey: 'Trickers'
  },
  {
    id: 2370,
    name: 'TriggerPoint',
    status: 'Visible',
    urlKey: 'TriggerPoint'
  },
  {
    id: 2633,
    name: 'Tsovet',
    status: 'Visible',
    urlKey: 'Tsovet'
  },
  {
    id: 2511,
    name: 'Turms',
    status: 'Visible',
    urlKey: 'Turms'
  },
  {
    id: 968,
    name: 'Turnbull & Asser',
    status: 'Visible',
    urlKey: 'Turnbull_and_Asser'
  },
  {
    id: 2171,
    name: 'Under Armour',
    status: 'Visible',
    urlKey: 'Under_Armour'
  },
  {
    id: 2780,
    name: 'Under Armour Sportswear',
    status: 'Visible',
    urlKey: 'Under_Armour_Sportswear'
  },
  {
    id: 1401,
    name: 'Undercover',
    status: 'Visible',
    urlKey: 'Undercover'
  },
  {
    id: 1255,
    name: 'Uniform Wares',
    status: 'Visible',
    urlKey: 'Uniform_Wares'
  },
  {
    id: 285,
    name: 'Valentino',
    status: 'Visible',
    urlKey: 'Valentino'
  },
  {
    id: 969,
    name: 'Valextra',
    status: 'Visible',
    urlKey: 'Valextra'
  },
  {
    id: 2608,
    name: 'Valstar',
    status: 'Visible',
    urlKey: 'Valstar'
  },
  {
    id: 1823,
    name: 'Vans',
    status: 'Visible',
    urlKey: 'Vans'
  },
  {
    id: 2641,
    name: 'Velva Sheen',
    status: 'Visible',
    urlKey: 'Velva_Sheen'
  },
  {
    id: 2417,
    name: 'Vetements',
    status: 'Visible',
    urlKey: 'Vetements'
  },
  {
    id: 2122,
    name: 'Viberg',
    status: 'Visible',
    urlKey: 'Viberg'
  },
  {
    id: 970,
    name: 'Vilebrequin',
    status: 'Visible',
    urlKey: 'Vilebrequin'
  },
  {
    id: 11,
    name: 'Visionaire',
    status: 'Visible',
    urlKey: 'Visionaire'
  },
  {
    id: 1814,
    name: 'visvim',
    status: 'Visible',
    urlKey: 'visvim'
  },
  {
    id: 2707,
    name: 'Wacko Maria',
    status: 'Visible',
    urlKey: 'Wacko_Maria'
  },
  {
    id: 1147,
    name: 'WANT LES ESSENTIELS',
    status: 'Visible',
    urlKey: 'WANT_LES_ESSENTIELS'
  },
  {
    id: 1834,
    name: 'William Lockie',
    status: 'Visible',
    urlKey: 'William_Lockie'
  },
  {
    id: 2519,
    name: 'WohnGeist',
    status: 'Visible',
    urlKey: 'WohnGeist'
  },
  {
    id: 2491,
    name: 'WOLF',
    status: 'Visible',
    urlKey: 'WOLF'
  },
  {
    id: 2063,
    name: 'Wooster + Lardini',
    status: 'ComingSoon',
    urlKey: 'Wooster_and_Lardini'
  },
  {
    id: 1353,
    name: 'Wooyoungmi',
    status: 'Visible',
    urlKey: 'Wooyoungmi'
  },
  {
    id: 563,
    name: 'Y-3',
    status: 'Visible',
    urlKey: 'Y_3'
  },
  {
    id: 2526,
    name: 'Ystudio',
    status: 'Visible',
    urlKey: 'Ystudio'
  },
  {
    id: 1815,
    name: 'Yuketen',
    status: 'Visible',
    urlKey: 'Yuketen'
  },
  {
    id: 2499,
    name: 'Zaha Hadid Design',
    status: 'Visible',
    urlKey: 'Zaha_Hadid_Design'
  },
  {
    id: 2505,
    name: 'Zenith',
    status: 'Visible',
    urlKey: 'Zenith'
  },
  {
    id: 1106,
    name: 'Zimmerli',
    status: 'Visible',
    urlKey: 'Zimmerli'
  }
];
