import supertest from 'supertest';
import server from '../server';

describe('Server', () => {

  it('should return 200 for the healthcheck route', (done) => {
    supertest(server)
      .get('/healthcheck')
      .expect(200)
      // Needed for jest as supertest end method doesn't place nicely out of the box
      // https://github.com/visionmedia/supertest/issues/352
      .end((err, res) => err ? done.fail(err) : done());
  });


  it('should 404 for a wrong route', (done) => {
    supertest(server)
      .get('/asdasd')
      .expect(404)
      .end((err, res) => err ? done.fail(err) : done());
  });

  afterAll(() => {
    server.close();
  });

});
