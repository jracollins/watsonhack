import path from 'path';
import express from 'express';
import React from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import { ServerRouter, createServerRenderContext } from 'react-router';
import { Provider } from 'react-redux';
import configureStore from 'app/redux/store';
import config from 'config/config.js';
import sourceMap from 'source-map-support';
import logErrors from 'server/errorHandling';
import App from 'app';
import bodyParser from 'body-parser';

sourceMap.install({
  environment: 'node'
});

// Initialising express app
const app = express().disable('x-powered-by').disable('etag');

import remoteHandlebars from 'express-remote-handlebars';

app.engine('handlebars', remoteHandlebars({
  layout: 'http://mrpxitefurniture-prod1.elasticbeanstalk.com/layouts/mrporter?locale=en-gb',
  cacheControl: 'max-age=600, stale-while-revalidate=86400'
}));
app.set('view engine', 'handlebars');
app.set('view cache', true);
app.set('views', `${__dirname}/views`);


// Serve static assets from generated static folder.
app.use(express.static(path.join(__dirname, '..', 'client')));


const credentials = {
  url: 'https://stream.watsonplatform.net/speech-to-text/api',
  password: '58rwp1wy0ml8',
  username: '8dfd73fc-fcb9-44bf-ac53-448ff7346f30'
};

const watson = require('watson-developer-cloud');

const stt = new watson.SpeechToTextV1(credentials);

const authService = new watson.AuthorizationV1(stt.getCredentials());


// Put health check and other non react powered routes here:
app.get('/token', (req, res, next) => {
  authService.getToken((err, token) => {
    if (err) {
      next(err);
    } else {
      res.send(token);
    }
  });
});


import AlchemyLanguageV1 from 'watson-developer-cloud/alchemy-language/v1';

const alchemy_language = new AlchemyLanguageV1({
  api_key: '355266491b345cda940733f6558d1db3373c9780'
});

const jsonParser = bodyParser.json();


app.post('/alchemy', jsonParser, (req, res, next) => {

  const text = req.body.text;
  console.log(text);
  const params = {
    text,
    extract: 'entities,keywords,doc-emotion',
    sentiment: 1,
  };

  alchemy_language.combined(params, (err, response) => {
    if (err) {
      console.log('error:', err);
      res.status(500).send(err);
    } else {
      console.log(JSON.stringify(response, null, 2));
      res.send(response);
    }

  });

});
// All other routes will be be matched by react-router here:

app.get('*', (req, res, next) => {
  const context = createServerRenderContext();
  const store = configureStore();

  // render the first time
  let markup = renderToString(
    <ServerRouter
      location={req.url}
      context={context}
    >
      {({ location }) => (
        <Provider store={store}>
          <App location={location} />
        </Provider>
          )}
    </ServerRouter>
);


  const result = context.getResult();

  if (result.redirect) {
    res.redirect(301, result.redirect.pathname);
  } else if (result.missed) {
    res.status(404).send();
  } else {

    markup = renderToString(
      <ServerRouter
        location={req.url}
        context={context}
      >
        {({ location }) => (
          <Provider store={store}>
            <App location={location} />
          </Provider>
            )}
      </ServerRouter>
    );

    res.render('main', {
      content: markup,
      scripts: '<script type="text/javascript" defer src="/vendor.bundle.js"></script><script type="text/javascript" defer src="/client.js"></script>',
      styles: '<link rel="stylesheet" href="/bundle.css">'

    });

  }
});


// Error catching
app.use(logErrors);

const server = app.listen(config.port, () => {
  console.log(`Server listening on port ${config.port}`);
});

export default server;
