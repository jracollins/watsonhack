import React from 'react';
import { render } from 'react-dom';
import { AppContainer as HotLoadAppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router';
import configureStore from 'app/redux/store';
import App from 'app';

const initializedStore = configureStore(window.state);

// Standard mount to DOM:
const rootElement = document.getElementsByClassName('sf-content')[0];

render((
  <HotLoadAppContainer>
    <BrowserRouter>
      {
        ({ location }) => (
          <Provider store={initializedStore}>
            <App location={location} />
          </Provider>
        )
      }
    </BrowserRouter>
  </HotLoadAppContainer>
), rootElement);

if (module.hot) {
  module.hot.accept('app', () => {
    render(
      <HotLoadAppContainer>
        <BrowserRouter>
          {
            ({ location }) => (
              <Provider store={initializedStore}>
                <App location={location} />
              </Provider>
            )
          }
        </BrowserRouter>
      </HotLoadAppContainer>,
      rootElement
    );
  });
}
