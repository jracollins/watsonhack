import etc from 'etc';

const testConfig = require('./test.json');
const devConfig = require('./development.json');
const prodConfig = require('./production.json');

const config = etc()
   .argv()
   .env()
   .add(process.env.NODE_ENV === 'test' ? testConfig : {})
   .add(process.env.NODE_ENV === 'development' ? devConfig : {})
   .add(process.env.NODE_ENV === 'production' ? prodConfig : {});


export default config.toJSON();
