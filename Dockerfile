FROM node:7

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
# COPY .npmrc .npmrc
COPY package.json /usr/src/app/
RUN npm install
# RUN rm -f .npmrc

# Bundle app source
COPY . /usr/src/app

RUN npm run prod

EXPOSE 8081
CMD [ "npm", "start" ]
