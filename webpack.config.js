const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const Visualizer = require('webpack-visualizer-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const WebpackNotifierPlugin = require('webpack-notifier');
const AssetsPlugin = require('assets-webpack-plugin');
const WebpackNodeServerPlugin = require('webpack-node-server-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');


// TESTING GOOGLE CLOSURE COMPILER & GZIPPING TO SEE SIZES
const CompressionPlugin = require('compression-webpack-plugin');
const ClosureCompilerPlugin = require('webpack-closure-compiler');

const nodeEnv = process.env.NODE_ENV || 'development';
const isProd = nodeEnv === 'production';

const sourcePath = path.join(__dirname, './src');
const clientOutputPath = path.join(__dirname, './build/client');
const serverOutputPath = path.join(__dirname, './build/server');

// CLI OUTPUT CONFIG:
// https://webpack.js.org/configuration/stats/
const stats = {
  assets: true,
  cached: false,
  children: true,
  chunks: false,
  chunkModules: false,
  chunkOrigins: false,
  errors: true,
  errorDetails: true,
  hash: true,
  modules: false,
  publicPath: false,
  reasons: false,
  source: false,
  timings: true,
  version: true,
  warnings: true
};

// Required for "externals" in server side webpack config
const nodeModules = {};
fs.readdirSync('node_modules')
  .filter(x => ['.bin'].indexOf(x) === -1)
  .forEach((mod) => {
		// Beacuse server side resolving of aliased modules doesn't really work smoothly
		// bundle react and preact into server side if required in the code.
    if (mod.startsWith('preact') || mod.startsWith('react') || mod.startsWith('inferno')) {
      return;
    }
    nodeModules[mod] = `commonjs ${mod}`;
  });

// DEFAULT PLUGINS
const clientPlugins = [
  new ExtractTextPlugin({
    filename: 'bundle.css',
    allChunks: true,
  }),
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity,
    filename: 'vendor.bundle.js'
  }),
  new webpack.DefinePlugin({
    'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
  }),
  new webpack.NamedModulesPlugin(),
  new WebpackMd5Hash(),
  new AssetsPlugin({ filename: 'build/assets.json', prettyPrint: true })
];

const serverPlugins = [
	// Banner plugin is used to add server side source maps
  // new webpack.BannerPlugin({ banner: 'require("source-map-support").install();', raw: true, entryOnly: false }),
	// Limit server to 1 file
  new CopyWebpackPlugin([{ from: `${sourcePath}/server/views`, to: `${serverOutputPath}/views` }], {}),
  new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 })
];

// PRODUCTION PLUGINS
if (isProd) {
  clientPlugins.push(
    new Visualizer({ filename: '../clientStats.html' }),
		new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false
      },
    }),
    // new ClosureCompilerPlugin({
    //   compiler: {
    //     language_in: 'ECMASCRIPT5',
    //     language_out: 'ECMASCRIPT5_STRICT',
    //     compilation_level: 'ADVANCED',
    //   },
    //   debug: false,
    //   jsCompiler: true,
    // }),
    new CompressionPlugin({
      asset: '../clientGzipped/[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.html$/
    })
  );

  serverPlugins.push(
		new webpack.optimize.OccurrenceOrderPlugin()
	);

} else {
    // DEV PLUGINS
  clientPlugins.push(
     new WebpackNotifierPlugin({ alwaysNotify: true })
    //  new webpack.HotModuleReplacementPlugin()
  );

  serverPlugins.push(
    new WebpackNodeServerPlugin(),
		new WebpackNotifierPlugin({ alwaysNotify: true })
		// new webpack.HotModuleReplacementPlugin()
	);
}

const include = [path.resolve('./src')];

const performance = {
  hints: isProd
  // assetFilter: assetFilename =>
	// 	// Function predicate that provides asset filenames
  //    assetFilename.endsWith('.css') || assetFilename.endsWith('.js')
};

// Allows switching between preact & inferno for production builds via env variables
let alias = {};
if (process.env.FRAMEWORK === 'preact') {
  console.log('Using preact');
  alias = {
    react: 'preact-compat',
    'react-dom': 'preact-compat',
    'react-redux': 'preact-redux',
    'react-dom/server': 'preact-compat/server'
  };
  include.push(path.resolve('node_modules/preact/src'), path.resolve('node_modules/preact-compat/src'), path.resolve('node_modules/preact-compat/server'));
}
if (process.env.FRAMEWORK === 'inferno') {
  console.log('Using inferno');
  alias = {
    react: 'inferno-compat',
    'react-dom/server': 'inferno-server',
    'react-dom': 'inferno-compat',
    'react-redux': 'inferno-redux'
  };
}

module.exports = [
  {
    name: 'server',
    context: sourcePath,
    target: 'node',
    node: {
      // Stops webpack overriding dirname and filename so relative path for serving assets works/
      __filename: false,
      __dirname: false
    },
    devtool: isProd ? 'source-map' : 'eval',
    entry: {
      js: './server/server.js',
    },
    externals: nodeModules,
    output: {
      path: serverOutputPath,
      filename: 'server.js',
      chunkFilename: '[name].js',
      pathinfo: true,
      publicPath: '/'
    },
    stats,
    plugins: serverPlugins,
    resolve: {
      alias,
      extensions: ['.js', '.jsx'],
      modules: [
        path.resolve(__dirname, 'node_modules'),
        sourcePath
      ]
    },
    performance,
    module: {
      rules: [
        {
          test: /\.(scss|css|html)$/,
          use: 'ignore-loader'
        },

        { test: /\.(json)$/,
          loader: 'json-loader'
        },
        {
          test: /\.(js|jsx)$/,
          include,
          use: [
            'babel-loader'
          ]
        },
      ],
    }
  },
  {
    name: 'client',
    target: 'web',
    devtool: isProd ? 'source-map' : 'eval',
    context: sourcePath,
    stats,
    entry: {
      js: './client/index.js',
      vendor: ['react', 'react-dom', 'react-redux', 'react-router', 'react-router-redux', 'redux-saga']
    },
    output: {
      path: clientOutputPath,
      filename: 'client.js',
      chunkFilename: '[name].js',
      publicPath: '/'
    },
    plugins: clientPlugins,
    resolve: {
      alias,
      extensions: ['.js', '.jsx'],
      modules: [
        path.resolve(__dirname, 'node_modules'),
        sourcePath
      ]
    },
    performance,
    module: {
      rules: [
        {
          test: /\.html$/,
          exclude: /node_modules/,
          loader: [{
            loader: 'file-loader',
            query: {
              name: '[name].[ext]'
            }
          }]
        },

        { test: /\.(json)$/,
          loader: 'json-loader'
        },
        {
          test: /\.(scss|css)$/,
          loader: ExtractTextPlugin.extract({
            fallbackLoader: 'style-loader',
            loader: [
              {
                loader: 'css-loader',
                query: {
                // modules: true,
                // sourceMap: false,
                // localIdentName: '[hash:base64:5]',
                },
              },
            // 'postcss-loader',
              {
                loader: 'sass-loader',
                query: {
                  sourceMap: false,
                }
              }
            ],
          })
        },
        {
          test: /\.(js|jsx)$/,
          include,
          loader: [
            {
              loader: 'babel-loader'
            }]
        }
      ],
    }
  }
];
